all: hl

hl: build-hl run-hl
debug: debug-hl run-hl

build-hl:
ifeq ($(OS),Windows_NT)
	haxelib install --always hl.dx.hxml && haxe hl.dx.hxml
else ifeq ($(shell uname -s),Darwin)
	haxelib install --always hl.sdl.hxml && haxe hl.sdl.hxml
endif

debug-hl:
	haxelib install --always hl.debug.hxml && haxe hl.debug.hxml

run-hl:
	hl bin/client.hl

ios:
	haxe -hl ../heaps-ios/out/main.c base.hxml -lib hlsdl
	haxe -hl ../heaps-ios/out/pak.hl -cp src -lib heaps -main hxd.fmt.pak.Build
	make -C ../heaps-ios clean
	make -C ../heaps-ios

ios-install:
	make -C ../heaps-ios install
