<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>5.4.0</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <true/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>spine</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>0</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">Basic</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">AnySize</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>data</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../../res/atlas/zombie1.atlas</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <false/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>0</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">None</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <false/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">Attack/attack0001.png</key>
            <key type="filename">Attack/attack0002.png</key>
            <key type="filename">Attack/attack0003.png</key>
            <key type="filename">Attack/attack0004.png</key>
            <key type="filename">Attack/attack0005.png</key>
            <key type="filename">Attack/attack0006.png</key>
            <key type="filename">Attack/attack0007.png</key>
            <key type="filename">Attack/attack0008.png</key>
            <key type="filename">Attack/attack0009.png</key>
            <key type="filename">Attack/attack0010.png</key>
            <key type="filename">Attack/attack0011.png</key>
            <key type="filename">Attack/attack0012.png</key>
            <key type="filename">Attack/attack0013.png</key>
            <key type="filename">Attack/attack0014.png</key>
            <key type="filename">Attack/attack0015.png</key>
            <key type="filename">Attack/attack0016.png</key>
            <key type="filename">Attack/attack0017.png</key>
            <key type="filename">Attack/attack0018.png</key>
            <key type="filename">Attack/attack0019.png</key>
            <key type="filename">Attack/attack0020.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>0.25</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>58,55,115,110</rect>
                <key>scale9Paddings</key>
                <rect>58,55,115,110</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Die/die0001.png</key>
            <key type="filename">Die/die0002.png</key>
            <key type="filename">Die/die0003.png</key>
            <key type="filename">Die/die0004.png</key>
            <key type="filename">Die/die0005.png</key>
            <key type="filename">Die/die0006.png</key>
            <key type="filename">Die/die0007.png</key>
            <key type="filename">Die/die0008.png</key>
            <key type="filename">Die/die0009.png</key>
            <key type="filename">Die/die0010.png</key>
            <key type="filename">Die/die0011.png</key>
            <key type="filename">Die/die0012.png</key>
            <key type="filename">Die/die0013.png</key>
            <key type="filename">Die/die0014.png</key>
            <key type="filename">Die/die0015.png</key>
            <key type="filename">Die/die0016.png</key>
            <key type="filename">Die/die0017.png</key>
            <key type="filename">Die/die0018.png</key>
            <key type="filename">Die/die0019.png</key>
            <key type="filename">Die/die0020.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>0.25</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>75,55,150,110</rect>
                <key>scale9Paddings</key>
                <rect>75,55,150,110</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Idle/idle0001.png</key>
            <key type="filename">Idle/idle0002.png</key>
            <key type="filename">Idle/idle0003.png</key>
            <key type="filename">Idle/idle0004.png</key>
            <key type="filename">Idle/idle0005.png</key>
            <key type="filename">Idle/idle0006.png</key>
            <key type="filename">Idle/idle0007.png</key>
            <key type="filename">Idle/idle0008.png</key>
            <key type="filename">Idle/idle0009.png</key>
            <key type="filename">Idle/idle0010.png</key>
            <key type="filename">Idle/idle0011.png</key>
            <key type="filename">Idle/idle0012.png</key>
            <key type="filename">Idle/idle0013.png</key>
            <key type="filename">Idle/idle0014.png</key>
            <key type="filename">Idle/idle0015.png</key>
            <key type="filename">Idle/idle0016.png</key>
            <key type="filename">Idle/idle0017.png</key>
            <key type="filename">Idle/idle0018.png</key>
            <key type="filename">Idle/idle0019.png</key>
            <key type="filename">Idle/idle0020.png</key>
            <key type="filename">Run/run0001.png</key>
            <key type="filename">Run/run0002.png</key>
            <key type="filename">Run/run0003.png</key>
            <key type="filename">Run/run0004.png</key>
            <key type="filename">Run/run0005.png</key>
            <key type="filename">Run/run0006.png</key>
            <key type="filename">Run/run0007.png</key>
            <key type="filename">Run/run0008.png</key>
            <key type="filename">Run/run0009.png</key>
            <key type="filename">Run/run0010.png</key>
            <key type="filename">Run/run0011.png</key>
            <key type="filename">Run/run0012.png</key>
            <key type="filename">Run/run0013.png</key>
            <key type="filename">Run/run0014.png</key>
            <key type="filename">Run/run0015.png</key>
            <key type="filename">Run/run0016.png</key>
            <key type="filename">Run/run0017.png</key>
            <key type="filename">Run/run0018.png</key>
            <key type="filename">Run/run0019.png</key>
            <key type="filename">Run/run0020.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>0.25</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>55,55,111,110</rect>
                <key>scale9Paddings</key>
                <rect>55,55,111,110</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>.</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
