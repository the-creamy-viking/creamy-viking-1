<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>5.5.0</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <true/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>spine</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>0</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>4096</int>
            <key>height</key>
            <int>4096</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">Basic</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">AnySize</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>data</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../../res/atlas/skeleton_king.atlas</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <false/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>0</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">None</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <false/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">Idle/idle0001.png</key>
            <key type="filename">Idle/idle0002.png</key>
            <key type="filename">Idle/idle0003.png</key>
            <key type="filename">Idle/idle0004.png</key>
            <key type="filename">Idle/idle0005.png</key>
            <key type="filename">Idle/idle0006.png</key>
            <key type="filename">Idle/idle0007.png</key>
            <key type="filename">Idle/idle0008.png</key>
            <key type="filename">Idle/idle0009.png</key>
            <key type="filename">Idle/idle0010.png</key>
            <key type="filename">Idle/idle0011.png</key>
            <key type="filename">Idle/idle0012.png</key>
            <key type="filename">Idle/idle0013.png</key>
            <key type="filename">Idle/idle0014.png</key>
            <key type="filename">Idle/idle0015.png</key>
            <key type="filename">Idle/idle0016.png</key>
            <key type="filename">Idle/idle0017.png</key>
            <key type="filename">Idle/idle0018.png</key>
            <key type="filename">Idle/idle0019.png</key>
            <key type="filename">Idle/idle0020.png</key>
            <key type="filename">die/die0001.png</key>
            <key type="filename">die/die0002.png</key>
            <key type="filename">die/die0003.png</key>
            <key type="filename">die/die0004.png</key>
            <key type="filename">die/die0005.png</key>
            <key type="filename">die/die0006.png</key>
            <key type="filename">die/die0007.png</key>
            <key type="filename">die/die0008.png</key>
            <key type="filename">die/die0009.png</key>
            <key type="filename">die/die0010.png</key>
            <key type="filename">die/die0011.png</key>
            <key type="filename">die/die0012.png</key>
            <key type="filename">die/die0013.png</key>
            <key type="filename">die/die0014.png</key>
            <key type="filename">die/die0015.png</key>
            <key type="filename">die/die0016.png</key>
            <key type="filename">die/die0017.png</key>
            <key type="filename">die/die0018.png</key>
            <key type="filename">die/die0019.png</key>
            <key type="filename">die/die0020.png</key>
            <key type="filename">die/die0021.png</key>
            <key type="filename">die/die0022.png</key>
            <key type="filename">die/die0023.png</key>
            <key type="filename">die/die0024.png</key>
            <key type="filename">die/die0025.png</key>
            <key type="filename">die/die0026.png</key>
            <key type="filename">die/die0027.png</key>
            <key type="filename">die/die0028.png</key>
            <key type="filename">die/die0029.png</key>
            <key type="filename">die/die0030.png</key>
            <key type="filename">die/die0031.png</key>
            <key type="filename">die/die0032.png</key>
            <key type="filename">die/die0033.png</key>
            <key type="filename">die/die0034.png</key>
            <key type="filename">die/die0035.png</key>
            <key type="filename">die/die0036.png</key>
            <key type="filename">die/die0037.png</key>
            <key type="filename">die/die0038.png</key>
            <key type="filename">die/die0039.png</key>
            <key type="filename">die/die0040.png</key>
            <key type="filename">die/die0041.png</key>
            <key type="filename">die/die0042.png</key>
            <key type="filename">die/die0043.png</key>
            <key type="filename">die/die0044.png</key>
            <key type="filename">die/die0045.png</key>
            <key type="filename">die/die0046.png</key>
            <key type="filename">die/die0047.png</key>
            <key type="filename">die/die0048.png</key>
            <key type="filename">hurt/hurt0001.png</key>
            <key type="filename">hurt/hurt0002.png</key>
            <key type="filename">hurt/hurt0003.png</key>
            <key type="filename">hurt/hurt0004.png</key>
            <key type="filename">hurt/hurt0005.png</key>
            <key type="filename">hurt/hurt0006.png</key>
            <key type="filename">hurt/hurt0007.png</key>
            <key type="filename">hurt/hurt0008.png</key>
            <key type="filename">hurt_ice/hurt_ice0001.png</key>
            <key type="filename">hurt_ice/hurt_ice0002.png</key>
            <key type="filename">hurt_ice/hurt_ice0003.png</key>
            <key type="filename">hurt_ice/hurt_ice0004.png</key>
            <key type="filename">hurt_ice/hurt_ice0005.png</key>
            <key type="filename">hurt_ice/hurt_ice0006.png</key>
            <key type="filename">hurt_ice/hurt_ice0007.png</key>
            <key type="filename">hurt_ice/hurt_ice0008.png</key>
            <key type="filename">idle_ice/idle_ice0001.png</key>
            <key type="filename">idle_ice/idle_ice0002.png</key>
            <key type="filename">idle_ice/idle_ice0003.png</key>
            <key type="filename">idle_ice/idle_ice0004.png</key>
            <key type="filename">idle_ice/idle_ice0005.png</key>
            <key type="filename">idle_ice/idle_ice0006.png</key>
            <key type="filename">idle_ice/idle_ice0007.png</key>
            <key type="filename">idle_ice/idle_ice0008.png</key>
            <key type="filename">idle_ice/idle_ice0009.png</key>
            <key type="filename">idle_ice/idle_ice0010.png</key>
            <key type="filename">idle_ice/idle_ice0011.png</key>
            <key type="filename">idle_ice/idle_ice0012.png</key>
            <key type="filename">idle_ice/idle_ice0013.png</key>
            <key type="filename">idle_ice/idle_ice0014.png</key>
            <key type="filename">idle_ice/idle_ice0015.png</key>
            <key type="filename">idle_ice/idle_ice0016.png</key>
            <key type="filename">idle_ice/idle_ice0017.png</key>
            <key type="filename">idle_ice/idle_ice0018.png</key>
            <key type="filename">idle_ice/idle_ice0019.png</key>
            <key type="filename">idle_ice/idle_ice0020.png</key>
            <key type="filename">run/run0001.png</key>
            <key type="filename">run/run0002.png</key>
            <key type="filename">run/run0003.png</key>
            <key type="filename">run/run0004.png</key>
            <key type="filename">run/run0005.png</key>
            <key type="filename">run/run0006.png</key>
            <key type="filename">run/run0007.png</key>
            <key type="filename">run/run0008.png</key>
            <key type="filename">run/run0009.png</key>
            <key type="filename">run/run0010.png</key>
            <key type="filename">run/run0011.png</key>
            <key type="filename">run/run0012.png</key>
            <key type="filename">run/run0013.png</key>
            <key type="filename">run/run0014.png</key>
            <key type="filename">run/run0015.png</key>
            <key type="filename">run/run0016.png</key>
            <key type="filename">run/run0017.png</key>
            <key type="filename">run/run0018.png</key>
            <key type="filename">run/run0019.png</key>
            <key type="filename">run/run0020.png</key>
            <key type="filename">run_ice/run_ice0001.png</key>
            <key type="filename">run_ice/run_ice0002.png</key>
            <key type="filename">run_ice/run_ice0003.png</key>
            <key type="filename">run_ice/run_ice0004.png</key>
            <key type="filename">run_ice/run_ice0005.png</key>
            <key type="filename">run_ice/run_ice0006.png</key>
            <key type="filename">run_ice/run_ice0007.png</key>
            <key type="filename">run_ice/run_ice0008.png</key>
            <key type="filename">run_ice/run_ice0009.png</key>
            <key type="filename">run_ice/run_ice0010.png</key>
            <key type="filename">run_ice/run_ice0011.png</key>
            <key type="filename">run_ice/run_ice0012.png</key>
            <key type="filename">run_ice/run_ice0013.png</key>
            <key type="filename">run_ice/run_ice0014.png</key>
            <key type="filename">run_ice/run_ice0015.png</key>
            <key type="filename">run_ice/run_ice0016.png</key>
            <key type="filename">run_ice/run_ice0017.png</key>
            <key type="filename">run_ice/run_ice0018.png</key>
            <key type="filename">run_ice/run_ice0019.png</key>
            <key type="filename">run_ice/run_ice0020.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>0.5</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>152,96,303,193</rect>
                <key>scale9Paddings</key>
                <rect>152,96,303,193</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">attack/attack0001.png</key>
            <key type="filename">attack/attack0002.png</key>
            <key type="filename">attack/attack0003.png</key>
            <key type="filename">attack/attack0004.png</key>
            <key type="filename">attack/attack0005.png</key>
            <key type="filename">attack/attack0006.png</key>
            <key type="filename">attack/attack0007.png</key>
            <key type="filename">attack/attack0008.png</key>
            <key type="filename">attack/attack0009.png</key>
            <key type="filename">attack/attack0010.png</key>
            <key type="filename">attack/attack0011.png</key>
            <key type="filename">attack/attack0012.png</key>
            <key type="filename">attack/attack0013.png</key>
            <key type="filename">attack/attack0014.png</key>
            <key type="filename">attack/attack0015.png</key>
            <key type="filename">attack/attack0016.png</key>
            <key type="filename">attack/attack0017.png</key>
            <key type="filename">attack/attack0018.png</key>
            <key type="filename">attack/attack0019.png</key>
            <key type="filename">attack/attack0020.png</key>
            <key type="filename">attack/attack0021.png</key>
            <key type="filename">attack/attack0022.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>0.5</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>152,115,303,230</rect>
                <key>scale9Paddings</key>
                <rect>152,115,303,230</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">change_to_ice/change_to_ice0001.png</key>
            <key type="filename">change_to_ice/change_to_ice0002.png</key>
            <key type="filename">change_to_ice/change_to_ice0003.png</key>
            <key type="filename">change_to_ice/change_to_ice0004.png</key>
            <key type="filename">change_to_ice/change_to_ice0005.png</key>
            <key type="filename">change_to_ice/change_to_ice0006.png</key>
            <key type="filename">change_to_ice/change_to_ice0007.png</key>
            <key type="filename">change_to_ice/change_to_ice0008.png</key>
            <key type="filename">change_to_ice/change_to_ice0009.png</key>
            <key type="filename">change_to_ice/change_to_ice0010.png</key>
            <key type="filename">change_to_ice/change_to_ice0011.png</key>
            <key type="filename">change_to_ice/change_to_ice0012.png</key>
            <key type="filename">change_to_ice/change_to_ice0013.png</key>
            <key type="filename">change_to_ice/change_to_ice0014.png</key>
            <key type="filename">change_to_ice/change_to_ice0015.png</key>
            <key type="filename">change_to_ice/change_to_ice0016.png</key>
            <key type="filename">change_to_ice/change_to_ice0017.png</key>
            <key type="filename">change_to_ice/change_to_ice0018.png</key>
            <key type="filename">change_to_ice/change_to_ice0019.png</key>
            <key type="filename">change_to_ice/change_to_ice0020.png</key>
            <key type="filename">change_to_ice/change_to_ice0021.png</key>
            <key type="filename">change_to_ice/change_to_ice0022.png</key>
            <key type="filename">change_to_ice/change_to_ice0023.png</key>
            <key type="filename">change_to_ice/change_to_ice0024.png</key>
            <key type="filename">change_to_ice/change_to_ice0025.png</key>
            <key type="filename">change_to_ice/change_to_ice0026.png</key>
            <key type="filename">change_to_ice/change_to_ice0027.png</key>
            <key type="filename">change_to_ice/change_to_ice0028.png</key>
            <key type="filename">change_to_ice/change_to_ice0029.png</key>
            <key type="filename">change_to_ice/change_to_ice0030.png</key>
            <key type="filename">change_to_ice/change_to_ice0031.png</key>
            <key type="filename">change_to_ice/change_to_ice0032.png</key>
            <key type="filename">change_to_ice/change_to_ice0033.png</key>
            <key type="filename">change_to_ice/change_to_ice0034.png</key>
            <key type="filename">change_to_ice/change_to_ice0035.png</key>
            <key type="filename">change_to_ice/change_to_ice0036.png</key>
            <key type="filename">change_to_ice/change_to_ice0037.png</key>
            <key type="filename">change_to_ice/change_to_ice0038.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>0.5</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>160,123,320,245</rect>
                <key>scale9Paddings</key>
                <rect>160,123,320,245</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>Idle</filename>
            <filename>change_to_ice</filename>
            <filename>idle_ice</filename>
            <filename>run</filename>
            <filename>run_ice</filename>
            <filename>die</filename>
            <filename>hurt</filename>
            <filename>hurt_ice</filename>
            <filename>attack</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
