package gm.en;

@:access(dn.heaps.slib.AnimManager)
class Pik extends Enemy {
    var entity: Entity_Pik;

    public function new(e: Entity_Pik, layer: ldtk.Layer_IntGrid_AutoLayer) {
        super(e.cx, e.cy, layer, false);
        entity = e;

        spr.anim.appendStateAnim('idle');
        spr.anim.appendStateAnim('pik', 0.5, () -> cd.has('attack'));

        cd.setS('repels', Math.POSITIVE_INFINITY);
        cd.setMs('idle', e.f_idle_delay, true);
    }

    override function update() {
        super.update();

        var pikDuration = getAnimDuration('pik');

        if (!cd.has('attack') && !cd.has('idle'))
            cd.setMs('attack', 2 * pikDuration, true, () -> cd.setMs('idle', pikDuration + entity.f_idle_delay_offset, true));

        dx = dy = 0;
    }

    override public function overlaps(e:Entity) {
        var animPct = spr.anim.spr.frame / spr.anim.spr.totalFrames();
        return cd.has('attack') && super.overlaps(e) && animPct > 0.1 && animPct < 0.9;
    }

    override public function hit(dmg:Int, from:Null<Entity>) {
        // noop
    }
}
