package gm.en;

class Zombie1 extends Enemy {
    public function new(e: Entity_Zombie1, layer: ldtk.Layer_IntGrid_AutoLayer) {
        super(e.cx, e.cy, layer, false);

        appendStateAnim('idle');
        appendStateAnim('run', () -> M.fabs(dx) > 0.001);
        appendStateAnim('attack', () -> cd.has('attack'));
        appendStateAnim('die', () -> cd.has('die'));

        var idle = getGroup('idle');
        if (idle != null) {
            this.hei = idle.maxHei;
        }
    }
}
