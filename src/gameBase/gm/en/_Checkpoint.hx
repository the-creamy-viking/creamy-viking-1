package gm.en;

class _Checkpoint extends LayerEntity {
    public static var ALL : Array<_Checkpoint> = [];
    public static var ACTIVE : _Checkpoint;
    public var id : Int;
    public var lavaDuration : Int;

    public function new(e: Entity_Checkpoint, layer: ldtk.Layer_IntGrid_AutoLayer) {
        super(e.cx, e.cy, layer);

        ALL.push(this);
        id = e.f_id;
        dir = e.f_dir;
        lavaDuration = e.f_lavaDuration;
    }

    public function isLava(): Bool {
        return lavaDuration > 0;
    }

    override function isAlive() {
        return false;
    }

    override public function dispose() {
        super.dispose();
        ALL.remove(this);
    }

    public static function load(id: Int) {
        for (c in ALL) {
            if (id == c.id) {
                c.set();
                break;
            }
        }
    }

    public function set() {
        ACTIVE = this;
        Game.ME.hero.initFromCheckpoint();
        Game.ME.level.initFromCheckpoint();
    }
}
