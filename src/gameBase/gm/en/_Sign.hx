package gm.en;

import h2d.Text.Align;
import h2d.Flow.FlowAlign;

class _Sign extends LayerEntity {
    public static var ALL : Array<_Sign> = [];

    public function new(e: Entity_Sign, layer: ldtk.Layer_IntGrid_AutoLayer) {
        super(e.cx, e.cy, layer);
        ALL.push(this);

        Game.ME.scroller.under(spr);

        // spr.setCenterRatio(0.5,0.9);
        spr.setTexture(Assets.getTile('sign').getTexture());

        var flow = new h2d.Flow(spr);
        flow.maxWidth = 96;
        flow.verticalAlign = FlowAlign.Middle;
        flow.x = -spr.tile.width / 2 + 20;
        flow.y = -140;
        var tf = new h2d.Text(Assets.fontSmall, flow);
        tf.textAlign = Align.Center;
        tf.text = e.f_text;
    }

    override function isAlive() {
        return false;
    }

    override public function dispose() {
        super.dispose();
        ALL.remove(this);
    }
}
