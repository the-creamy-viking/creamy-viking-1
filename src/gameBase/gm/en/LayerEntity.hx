package gm.en;

class LayerEntity extends Entity {
    public var defaultLayer: ldtk.Layer_IntGrid_AutoLayer;

    public function new(x:Int, y:Int, defaultLayer:ldtk.Layer_IntGrid_AutoLayer=null) {
        super(x, y);

        this.defaultLayer = defaultLayer;

        if (defaultLayer != null) {
            var kind = defaultLayer == level.layers.get(Level.COLLISIONS_GRASS_LAYER) ? 'grass' : 'lava';
            game.entities.get(kind).push(this);
        }
    }

    function hasCollision(x: Int, y: Int, layer:ldtk.Layer_IntGrid_AutoLayer=null): Bool {
        return level.hasCollision(x, y, layer != null ? layer : defaultLayer);
    }
}
