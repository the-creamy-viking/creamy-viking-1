package gm.en;

class Skeleton_King extends Enemy {
    public static var ME : Skeleton_King;
    public static var ATTACK_DELAY = 750;
    public var uiLife : ui.Life;
    private var iceMode : Bool = false;
    private var zombatCount : Int = 0;
    private var zombatMaxCount : Int = 0;

    public function new(e: Entity_SkeletonKing, layer: ldtk.Layer_IntGrid_AutoLayer) {
        super(e.cx, e.cy, layer, false);
        ME = this;

        spr.setCenterRatio(0.5, 0.97);

        appendStateAnim('idle');
        appendStateAnim('idle_ice', () -> iceMode);
        appendStateAnim('run', () -> M.fabs(dx) > 0.001);
        appendStateAnim('run_ice', () -> iceMode && M.fabs(dx) > 0.001);
        appendStateAnim('attack', () -> iceMode && cd.has('attack'));
        appendStateAnim('hurt', () -> cd.has('hurt'));
        appendStateAnim('hurt_ice', () -> iceMode && cd.has('hurt'));

        var idle = getGroup('idle');
        if (idle != null) {
            this.hei = idle.maxHei;
        }

        initLife(#if debug 6 #else 20 #end);
        var padding = 30;
        uiLife = new ui.Life(padding, padding, game.w() - 2 * padding, padding);
        attackRadiusScale = 1.3;
        radiusRatio = 0.9;
        zombatMaxCount = 3 + M.rand(5);
    }

    override function onDie() {
        var ms = this.getAnimDuration('die');

        for (e in Enemy.ALL)
            e.kill(game.entity);

        this.uiLife.hide();
        for (movieBar in game.movieBars)
            movieBar.stop();

        spr.anim.removeAllStateAnims();
        spr.anim.play('die');

        cd.cdList = [];
        cd.setMs('die', ms, () -> destroyed = true);

        haxe.Timer.delay(() -> game.leaderboard.add(), 2500);
    }

    override function update() {
        if (cd.has('die'))
            return;

        super.update();

        if (!cd.has('bat') && cd.has('follow')) {
            if (++zombatCount == zombatMaxCount)
                new gm.en.SuperZombat(cx + 1, cy, level.layers.get(Level.COLLISIONS_LAVA_LAYER));
            else
                new gm.en.Zombat(cx, cy, level.layers.get(Level.COLLISIONS_LAVA_LAYER));
            cd.setMs('bat', 1000 * (1 + Math.random() * 3));
        }
    }

    override function onAttackDelay() {
        if (iceMode) super.onAttackDelay();
    }

    override function hit(dmg:Int, from:Null<Entity>) {
        if (!iceMode && !Std.isOfType(from, gm.en.SuperZombat))
            return;

        super.hit(dmg, from);

        var lifeRatio = life / maxLife;

        if (!iceMode && !cd.has('change_to_ice') && lifeRatio <= 0.5) {
            cd.setMs('change_to_ice', this.getAnimDuration('change_to_ice'));
            spr.anim.play('change_to_ice');
            spr.anim.onEnd(() -> {
                iceMode = true;
                radiusRatio = 1.5;
            });
        }

        uiLife.set(lifeRatio);
        uiLife.dissociate();
    }
}
