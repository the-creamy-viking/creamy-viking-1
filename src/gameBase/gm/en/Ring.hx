package gm.en;

class Ring extends MotionEntity {
    public static var ALL : Array<Ring> = [];
    public static var ACTIVE : Ring;
    public var lavaDuration : Int;

    public function new(e: Entity_Ring, layer: ldtk.Layer_IntGrid_AutoLayer) {
        super(e.cx, e.cy, layer, false);

        appendStateAnim('idle', null, 0.7);
        ALL.push(this);
        lavaDuration = e.f_lavaDuration;
    }

    override function isAlive() {
        return false;
    }

    override public function dispose() {
        super.dispose();
        ALL.remove(this);
    }
}
