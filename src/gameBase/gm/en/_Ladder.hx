package gm.en;

class _Ladder extends LayerEntity {
    public static var ALL : Array<_Ladder> = [];
    public static var ACTIVE : _Ladder;

    public function new(e: Entity_Ladder, layer: ldtk.Layer_IntGrid_AutoLayer) {
        super(e.cx, e.cy, layer);
        ALL.push(this);

        Game.ME.scroller.under(spr);
        spr.set(Assets.getLib('_ladder'));
        spr.anim.appendStateAnim('ladder');
    }

    override function isAlive() {
        return false;
    }

    override public function dispose() {
        super.dispose();
        ALL.remove(this);
    }
}
