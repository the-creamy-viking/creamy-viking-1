package gm.en;

class Zombat extends Enemy {
    private var cos: Float = 0;
    private var flyDuration: Float = 0;

    public function new(cx: Int, cy: Int, layer: ldtk.Layer_IntGrid_AutoLayer) {
        super(cx, cy, layer, false);

        appendStateAnim('fly');
        appendStateAnim('attack', () -> cd.has('follow'));
        appendStateAnim('die', () -> cd.has('die'));

        var idle = getGroup('fly');
        if (idle != null) {
            this.hei = idle.maxHei;
        }

        gravityMul = 0;
        cos = Math.random() * 2 * Math.PI;
        flyDuration = this.getAnimDuration('fly');
    }

    override function preUpdate() {
        super.preUpdate();

        if (!cd.has('follow')) {
            dy = Math.cos(cos) / 30;
            cos += Math.PI / (flyDuration * tmod * Const.FPS / 1000);
        }
    }
}
