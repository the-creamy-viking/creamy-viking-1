package gm.en;

import dn.heaps.slib.SpriteLib.LibGroup;

@:access(dn.heaps.slib.AnimManager)
class MotionEntity extends LayerEntity {
    public static var ATTACK_DELAY = 400;

    public var sprLib : dn.heaps.slib.SpriteLib;

    // Resulting coordinates
    public var xx(get,never) : Int; inline function get_xx() return Std.int(spr.x);
    public var yy(get,never) : Int; inline function get_yy() return Std.int(spr.y);

    public var gravityMul = 1.0;

    public var hero(get,never) : gm.en.Hero; inline function get_hero() return game.hero;

    public var onGround(get,never): Bool;
    inline function get_onGround() return dy==0 && hasCollision(cx,cy+1) && yr==1;
    public var underCeil(get,never): Bool;
    inline function get_underCeil() return cy > 0 && dy==0 && hasCollision(cx,cy-1) && yr==1;

    public var cdEreg = new EReg('(\\w+\\|\\d:\\s)([\\w.]+)(\\/[\\w.]+)', 'g');

    public var attackRadius(default,set) = 0.;
    var attackRadiusScale: Float = 1;
    inline function set_attackRadius(v) { invalidateDebugBounds=true; return attackRadius=v; }

    public var radiusRatio: Float = 1.5;
    public var invicibilityOffset = 1000;
    public var speeds: Map<String, Float> = [];
    public var atlasTile: String;

    public function new(x:Int, y:Int, layer: ldtk.Layer_IntGrid_AutoLayer=null, zeroBased:Bool=true) {
        super(x, y, layer);

        if (atlasTile == null)
            atlasTile = Type.getClassName(Type.getClass(this)).split('.').pop().toLowerCase();

        sprLib = Assets.getLib(atlasTile, zeroBased);

        if (sprLib == null)
            throw 'Unknown atlas tile "${atlasTile}"';

        spr.set(sprLib);
        spr.setCenterRatio(0.5,0.9);
        spr.anim.setGlobalSpeed(Const.FIXED_UPDATE_FPS / Const.FPS);

        // radius = Const.GRID*0.3;
    }

    override function preUpdate() {
        super.preUpdate();

        // Y
        if( !onGround )
            dy += gravityMul*Const.GRAVITY * tmod;
    }

    override public function update() { // runs at an unknown fps
        super.update();

        #if debug
        if (ui.Console.ME.hasFlag("debug")) {
            if (!entityVisible)
                debugLabel.text = '';
            else if( ui.Console.ME.hasFlag('cd') ) {
                if (debugLabel != null) {
                    debugLabel.text = this.uid + '\n' + debugLabel.text.split('\n').slice(0, 1).concat(cd.debug().split('\n').filter(
                        cd -> cd.substring(0, 1) != '_'
                    ).map(
                        cd -> cdEreg.match(cd) ? cdEreg.matched(1) + StringTools.rpad(cdEreg.matched(2), '0', 4) + cdEreg.matched(3) : cd
                    )).join('\n');
                }
            }
        }
        #end
    }

    override public function postUpdate() {
        super.postUpdate();

        this.flip();
    }

    public function flip() {
        if (dir == 1 && dx < 0 || dir == -1 && dx > 0)
            dir *= -1;
    }

    override public function onPreStepX() {
        var xrmin = 0;
        var xrmax = 1;
        if (hasCollision(cx - 1, cy) && xr <= xrmin) {
            xr = xrmin;
            dx *= Math.pow(0.5,tmod);
        }
        if (hasCollision(cx + 1, cy) && xr >= xrmax) {
            xr = xrmax;
            dx *= Math.pow(0.5,tmod);
        }
    }

    override public function onPreStepY() {
        var yrmin = 0;
        var yrmax = 1;
        if (hasCollision(cx, cy - 1) && yr <= yrmin) {
            yr = yrmin;
            dy = 0;
        }
        if (hasCollision(cx, cy + 1) && yr >= yrmax) {
            yr = yrmax;
            dy = 0;
        }
    }

    override public function isAlive() {
        return super.isAlive() && life>0;
    }

    public inline function distCaseX(e:Entity) return M.fabs( (cx+xr) - (e.cx+e.xr) );
    public inline function distCaseY(e:Entity) return M.fabs( (cy+yr) - (e.cy+e.yr) );
    public inline function distPxX(e:Entity) return M.fabs( sprX - e.sprX );
    public inline function distPxY(e:Entity) return M.fabs( sprY - e.sprY );

    override function canSeeThrough(x,y) {
        return (x==cx && y==cy) || !hasCollision(x,y);
    }

    public function overlaps(e:Entity) {
        // Fast distance check
        if (e!=this && !cd.has('overlaps') && !e.cd.has('overlaps') && Math.abs(cx-e.cx) <= 2 && Math.abs(cy-e.cy) <= 2) {
            var maxDist = innerRadius+e.innerRadius;
            var overlaps = M.distSqr(e.sprX, e.sprY, sprX, sprY) <= maxDist * maxDist;

            if (overlaps) {
                cd.setF('overlaps', 10);
                e.cd.setF('overlaps', 10);
            }

            return overlaps;
        }

        return false;
    }

    public function overlapsAttack(e:MotionEntity) {
        // Fast distance check
        if (e!=this && !cd.has('overlapsAttack') && Math.abs(cx-e.cx) <= 2 && Math.abs(cy-e.cy) <= 2) {
            var maxDist = innerRadius+e.attackRadius;
            var attackRadiusOffset = e.attackRadius / 2;
            var overlaps = M.distSqr(e.sprX + (e.dir > 0 ? attackRadiusOffset : -attackRadiusOffset), e.sprY, sprX, sprY) <= maxDist * maxDist;

            if (overlaps) {
                cd.setF('overlapsAttack', 10);
            }

            return overlaps;
        }

        return false;
    }

    public function repels(e:Entity) {
        // Fast distance check
        if( e!=this && !cd.has('repels') && !e.cd.has('repels') && Math.abs(cx-e.cx) <= 2 && Math.abs(cy-e.cy) <= 2 ) {
            // Real distance check
            var dist = distPx(e);
            if( dist <= innerRadius+e.innerRadius ) {
                cd.setMs('repelsDuration', 1000);
                cd.setF('repels', 10);
                e.cd.setF('repels', 10);
                var ang = Math.atan2(e.sprY-sprY, e.sprX-sprX);
                var force = 0.2 * tmod;
                var repelPower = (innerRadius+e.innerRadius - dist) / (innerRadius+e.innerRadius);
                // var repelPower = 1 - dist / (radius+e.radius);
                dx -= Math.cos(ang) * repelPower * force;
                dy -= Math.sin(ang) * repelPower * force;

                if (e != Skeleton_King.ME) {
                    e.dx += Math.cos(ang) * repelPower * force;
                    e.dy += Math.sin(ang) * repelPower * force;
                }
            }
        }
    }

    override function onDamage(dmg:Int, from:Null<Entity>) {
        super.onDamage(dmg, from);

        cd.setMs('hurt', this.getAnimDuration('hurt'));
    }

    override function onDie() {
        var ms = this.getAnimDuration('die');

        cd.cdList = [];
        cd.setMs('die', ms, () -> destroy());
    }

    function getAnimDuration(group: String): Float {
        var factor = speeds.exists(group) ? speeds.get(group) : 1;
        var group = getGroup(group);
        return group == null ? 0 : 1000 * group.frames.length / Const.FIXED_UPDATE_FPS / factor;
    }

    function getGroup(group: String): Null<LibGroup> {
        return sprLib.getGroup(group);
    }

    function appendStateAnim(group: String, ?condition:Void->Bool, speed: Float = 1) {
        if (speed != 1)
            speeds.set(group, speed);

        spr.anim.appendStateAnim(group, speed, condition);
    }

    function follow(p: tools.LPoint, speed: Float = 0.001) {
        var x = Const.GRID * p.cxf;
        var y = Const.GRID * p.cyf;
        if (distPx(null, x, y) > innerRadius * radiusRatio + game.hero.innerRadius) {
            if (x < sprX) {
                dx -= speed*tmod;
            } else if (x > sprX) {
                dx += speed*tmod;
            }
            if (y < sprY) {
                dy -= speed*tmod;
            } else if (x > sprY) {
                dy += speed*tmod;
            }
        } else {
            if (!cd.has('attackDelay'))
                cd.setMs('attackDelay', ATTACK_DELAY, () -> this.onAttackDelay());
        }
    }

    function onAttackDelay() {
        this.attack();
    }

    function attack() {
        if (!cd.has('attack')) {
            var attackDuration = this.getAnimDuration('attack');
            cd.setMs('attack', attackDuration, () -> attackRadius = 0);
            cd.setMs('attackEnd', attackDuration / 3, () -> attackRadius = innerRadius * attackRadiusScale);
        }
    }

    override function hit(dmg:Int, from:Null<Entity>) {
        if (!cd.has('hit')) {
            cd.setMs('hit', this.getAnimDuration('hurt') + invicibilityOffset);
            super.hit(dmg, from);
        }
    }

    override function renderDebugBounds() {
        super.renderDebugBounds();

        var c = Color.makeColorHsl((uid%20)/20, 1, 1);

        // AttackRadius
        debugBounds.lineStyle(1, c, 0.8);
        var attackRadiusOffset = attackRadius / 2;
        debugBounds.drawCircle(dir > 0 ? attackRadiusOffset : -attackRadiusOffset, -attackRadius, attackRadius);
    }
}
