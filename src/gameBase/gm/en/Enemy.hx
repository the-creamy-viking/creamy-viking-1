package gm.en;

class Enemy extends MotionEntity {
    static var ALL : Array<Enemy> = [];
    var point: tools.LPoint;

    public function new(x:Int, y:Int, layer: ldtk.Layer_IntGrid_AutoLayer, zeroBased:Bool=true) {
        super(x,y, layer, zeroBased);
        ALL.push(this);

        initLife(1);
    }

    override function update() {
        super.update();

        if (!entityVisible)
            return;

        var duration = 1 + M.rand(4000) / 1000;

        if ((this == gm.en.Skeleton_King.ME && game.hero.inBossZone)|| canSee(hero)) {
            point = hero.createPoint();
            cd.setS('follow', duration);
        }

        if (cd.has('follow')) {
            follow(point, this == gm.en.Skeleton_King.ME ? 0.001 : 0.002);
            return;
        }

        if (cd.has('move')) {
            if (hasAffect(MoveRight)) {
                dx += 0.001*tmod;
            } else if (hasAffect(MoveLeft)) {
                dx -= 0.001*tmod;
            }
            return;
        } else if (M.rand(200) == 0) {
            cd.setMs('move', 1000 * duration);
            var rnd = M.rand(2);
            if (rnd == 0) {
                setAffectS(MoveRight, duration);
            } else {
                setAffectS(MoveLeft, duration);
            }
            return;
        }
    }

    override public function dispose() {
        super.dispose();
        ALL.remove(this);
    }

    function canSee(e: Entity, maxDist: Int = 10, fastMaxDist: Int = 2): Bool {
        var distX = distCaseX(e);
        var distY = distCaseY(e);

        return e.isAlive() && (
            (e.cy == cy && distX < fastMaxDist && distY < fastMaxDist)
            || (distX < maxDist && distY < maxDist && isFacing(e) /*&& sightCheck(e)*/)
        );
    }

    function isFacing(e: Entity): Bool {
        return (e.cx < cx && dir < 0) || (e.cx > cx && dir > 0);
    }
}
