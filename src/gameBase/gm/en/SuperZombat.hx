package gm.en;

class SuperZombat extends Zombat {
    override function onDie() {
        super.onDie();

        gm.en.Skeleton_King.ME.hit(Std.int(gm.en.Skeleton_King.ME.maxLife / 2), this);
    }
}
