package gm.en;

import haxe.Timer;

using Lambda;

@:access(dn.heaps.slib.AnimManager)
class Hero extends MotionEntity {
    public var uiLife : ui.Life;
    public var inBossZone : Bool = false;

    var canAttack: Bool = false;

    public function new(e: Entity_Hero) {
        super(e.cx, e.cy, null, false);

        spr.setCenterRatio(0.4, 0.9);

        appendStateAnim('idle');
        appendStateAnim('run', () -> M.fabs(dx) > 0.01);
        appendStateAnim('jump', () -> level != null && level.col != null && !onGround && cd.has('jumpForce'));
        appendStateAnim('attack', () -> cd.has('attack'), 2);
        appendStateAnim('hurt', () -> cd.has('hurt'));
        appendStateAnim('die', () -> cd.has('die'));

        var idle = getGroup('idle');
        if (idle != null) {
            this.hei = idle.maxHei;
        }

        initLife(5);

        var wid = 250;
	    var hei = 10;
        uiLife = new ui.Life(50, game.h() - 50 + hei * 0.5, wid, hei);
        uiLife.show();

        level.music.register(hxd.Res.music.music1_3, () -> hasCollision(cx, cy, level.layers.get(Level.BOSS_ZONE_LAVA_LAYER)));

        camera.trackEntity(this, true);
		camera.clampToLevelBounds = true;
        camera.centerOnTarget();
        camera.enableDebugBounds();
    }

    public function initFromCheckpoint() {
        cx = _Checkpoint.ACTIVE.cx;
        cy = _Checkpoint.ACTIVE.cy;
        xr = _Checkpoint.ACTIVE.xr;
        yr = _Checkpoint.ACTIVE.yr;
        dir = _Checkpoint.ACTIVE.dir;
    }

    override function update() {
        super.update();

        #if debug
        if (Console.ME.isActive())
            return;
        #end

        if (!isAlive())
            return;

        if (cy >= 0 && !level.isValid(cx, cy))
            kill(this);

        this.move();
        this.attack();

        for (e in Enemy.ALL.filter((entity) -> entity.entityVisible == true).array()) {
            if (e.isAlive()) {
                if (isAlive())
                    this.repels(e);

                if (cd.has('attack') && e.overlapsAttack(this))
                    e.hit(1, this);
                if (e.cd.has('attack') && this.overlapsAttack(e))
                    this.hit(1, e);
                if (e.overlaps(this))
                    this.hit(1, e);
            }
        }

        for (cp in _Checkpoint.ALL.filter((entity) -> entity.entityVisible == true).array())
            if (this.overlaps(cp) && _Checkpoint.ACTIVE != cp)
                this.save(cp);

        for (r in gm.en.Ring.ALL.filter((entity) -> entity.entityVisible == true).array())
            if (xr > 0.25 && xr < 0.75 && this.overlaps(r))
                this.hide(r);

        gravityMul = 1;
        for (l in _Ladder.ALL.filter((entity) -> entity.entityVisible == true).array())
            if (this.overlaps(l))
                gravityMul = 0;

        if (!inBossZone && hasCollision(cx, cy, level.layers.get(Level.BOSS_ZONE_LAVA_LAYER))) {
            inBossZone = true;
            camera.clampFromRight(2);
            game.fog.destroy();
            game.boss.uiLife.show();
            level.music.swap();
            for (movieBar in game.movieBars)
               movieBar.start();
        }
    }

    function move() {
        if( Interactive.ME.leftDown() )
            dx -= 0.0105*tmod;

        if( Interactive.ME.rightDown() )
            dx += 0.0105*tmod;

        if( onGround || gravityMul == 0 )
            cd.setMs('onGround', 100);

        if ( !underCeil && Interactive.ME.aDown() ) {
            if ( cd.has('onGround') ) {
                dy = -0.07;
                cd.setMs('jumpForce', 430);
            }
            if ( cd.has('jumpForce') )
                dy -= 0.05 * cd.getRatio('jumpForce') * tmod;
        }
    }

    override function onDie() {
        var ms = this.getAnimDuration('die');

        spr.anim.removeAllStateAnims();
        spr.anim.play('die');

        cd.cdList = [];
        cd.setMs('die', ms, () -> {
            destroyed = true;
            Timer.delay(() -> new ui.GameOver(), 1000);
        });
    }

    override function attack() {
        var wasAttacking = canAttack;
        canAttack = Interactive.ME.bDown();

        if (!wasAttacking && canAttack)
            super.attack();
    }

    override function hit(dmg:Int, from:Null<Entity>) {
        super.hit(dmg, from);

        uiLife.set(life / maxLife);
        uiLife.blink();
        game.fog.blink(0x6B0000, 0.25);
    }

    override public function flip() {
        if (
            (dir == 1 && Interactive.ME.leftDown() && !Interactive.ME.rightDown()) ||
            (dir == -1 && Interactive.ME.rightDown() && !Interactive.ME.leftDown())
        )
            dir *= -1;
    }

    function save(cp: _Checkpoint) {
        if (_Checkpoint.ACTIVE != cp)
            _Checkpoint.ACTIVE = cp;
    }

    function hide(r: Ring) {
        if (r != gm.en.Ring.ACTIVE) {
            // TODO move hero to nearest valid grid coordinates
            gm.en.Ring.ACTIVE = r;
            level.swap();
        }
    }

    public function addBonusLife(v) {
        life += v;
        maxLife += v;
        uiLife.set(life / maxLife);
    }
}
