/*
* Copyright (c) 2019 The Creamy Viking
*/

package gm;

import hxd.snd.Channel;
import hxd.res.Sound;

@:access(hxd.snd.Channel)
class MusicManager extends dn.Process {
    public static var MAX_VOLUME : Float = #if debug 0.01 #else 0.5 #end;

    public var volume(get,set) : Float; inline function get_volume() return this.channel.volume;

    private var channel: Channel;
    private var mutedVolume: Float = -1;
    private var musics: Array<Music> = [];

    public function new(): Void {
        super(Game.ME);
    }

    public function register(sound: Sound, ?condition: Void -> Bool): Void {
        if (condition == null)
            condition = () -> true;

        musics.push(new Music(sound, condition));
    }

    public function play(?loop: Bool = false, ?volume: Float = 0.): Void {
        var sound = this.getSound();
        if (sound == null)
            return;

        this.channel = sound.play(loop, volume);
    }

    public function stop(): Void {
        for (music in musics)
            music.sound.stop();

        hxd.snd.Manager.get().dispose();
    }

    public function set_volume(v: Float): Float {
        this.channel.volume = v;
        MusicManager.MAX_VOLUME = v;
        return v;
    }

    public function mute(): Void {
        if (this.channel != null) {
            this.mutedVolume = this.channel.volume;
            this.channel.volume = 0;
        }
    }

    public function unmute(): Void {
        if (this.channel != null) {
            this.channel.volume = this.mutedVolume;
            this.mutedVolume = -1;
        }
    }

    public function swap(): Void {
        var sound = this.getSound();
        if (sound == null || this.channel == null)
            return;

        var c = sound.play(this.channel.loop, this.channel.volume);
        c.position = this.channel.position;

        channel.stop();

        channel = c;
    }

    override function pause(): Void {
        if (this.channel != null)
            this.channel.pause = true;
    }

    override function resume(): Void {
        if (this.channel != null)
            this.channel.pause = false;
    }

    override function update(): Void {
        super.update();

        if (this.channel != null && this.mutedVolume == -1 && this.channel.volume < MusicManager.MAX_VOLUME)
            this.channel.volume = M.fmin(1, this.channel.volume + 0.01);
    }

    override function onDispose() {
        super.onDispose();

        this.stop();
    }

    function getSound(): Sound {
        for (music in this.musics)
            if (music.condition())
                return music.sound;

        return null;
    }
}

private class Music {
    public var sound : Sound;
    public var condition : Void->Bool;

    public function new(_sound: Sound, ?_condition: Void->Bool) {
        sound = _sound;
        condition = _condition;
    }
}
