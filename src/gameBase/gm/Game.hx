package gm;

import gm.en.Ring;
import gm.en._Checkpoint;
import hxd.Key;
import dn.Process;

class Game extends dn.Process {
    public static var ME : Game;

    /** Game controller (pad or keyboard) **/
    public var ca : dn.heaps.Controller.ControllerAccess;

    /** Particles **/
    public var fx : Fx;

    /** Basic viewport control **/
    public var camera : Camera;

    /** Container of all visual game objects. Ths wrapper is moved around by Camera. **/
    public var scroller : h2d.Layers;

    /** Level data **/
    public var level : Level;

    /** UI **/
    public var hud : ui.Hud;
    public var fog : ui.Fog;
    public var leaderboard : ui.Leaderboard;
    public var movieBars : Array<ui.MovieBar>;

    public var music : gm.MusicManager;

    /** Slow mo internal values**/
    var curGameSpeed = 1.0;
    var slowMos : Map<String, { id:String, t:Float, f:Float }> = new Map();

    /** LEd world data **/
    public var world : World;

    public var hero : gm.en.Hero;
    public var boss : gm.en.Skeleton_King;
    public var entity: Entity;

    public var entities : Map<String, Array<Entity>> = [
        'grass' => [],
        'lava' => [],
    ];

    public static var TIME : Float = 0;
    public static var DEATH : Int = 0;
    public static var DIFFICULTY : Int = 2; // 1 Easy, 2 Normal

	public var app(get,never) : App; inline function get_app() return App.ME;

    public function new() {
        super(App.ME);

        ME = this;
        ca = App.ME.controller.createAccess("game");
        ca.setLeftDeadZone(0.2);
        ca.setRightDeadZone(0.2);
        createRootInLayers(App.ME.root, Const.DP_BG);

        scroller = new h2d.Layers();
        root.add(scroller, Const.DP_BG);
        scroller.filter = new h2d.filter.Nothing(); // force rendering for pixel perfect

        world = new World();
        fx = new Fx();
        camera = new Camera();
        music = new gm.MusicManager();
        hud = new ui.Hud();
        fog = new ui.Fog();
        leaderboard = new ui.Leaderboard();
        movieBars = [
            new ui.MovieBar(1),
            new ui.MovieBar(-1),
        ];
        new Interactive();

        startLevel();
    }

    public static inline function exists() {
		return ME!=null && !ME.destroyed;
	}

    /** Load a level **/
    function startLevel() {
        if( level!=null )
            level.destroy();
        fx.clear();
        for(e in Entity.ALL)
            e.destroy();
        garbageCollectEntities();

        if (Boot.ME.s2d.width > 1280)
            camera.zoom = Boot.ME.s2d.width / 1280;

        hero = new gm.en.Hero(world.all_levels.Grass.l_EntitiesGrass.all_Hero[0]);
        level = new Level(world.all_levels.Grass);
        entity = new Entity(0, 0);

        var grass = level.layers.get(Level.COLLISIONS_GRASS_LAYER);
        var lava = level.layers.get(Level.COLLISIONS_LAVA_LAYER);

        for (ring in world.all_levels.Grass.l_EntitiesGrass.all_Ring)
            new gm.en.Ring(ring, grass);

        for (checkpoint in world.all_levels.Grass.l_EntitiesGrass.all_Checkpoint)
            new gm.en._Checkpoint(checkpoint, grass);

        for (pik in world.all_levels.Grass.l_EntitiesGrass.all_Pik)
            new gm.en.Pik(pik, grass);

        for (ring in world.all_levels.Grass.l_EntitiesLava.all_Ring)
            new gm.en.Ring(ring, lava);

        for (checkpoint in world.all_levels.Grass.l_EntitiesLava.all_Checkpoint)
            new gm.en._Checkpoint(checkpoint, lava);

        for (pik in world.all_levels.Grass.l_EntitiesLava.all_Pik)
            new gm.en.Pik(pik, lava);

        for (zombie1 in world.all_levels.Grass.l_EntitiesLava.all_Zombie1)
            new gm.en.Zombie1(zombie1, lava);

        for (zombie2 in world.all_levels.Grass.l_EntitiesLava.all_Zombie2)
            new gm.en.Zombie2(zombie2, lava);

        for (zombat in world.all_levels.Grass.l_EntitiesLava.all_Zombat)
            new gm.en.Zombat(zombat.cx, zombat.cy, lava);

        for (ladder in world.all_levels.Grass.l_EntitiesLava.all_Ladder)
            new gm.en._Ladder(ladder, lava);

        boss = new gm.en.Skeleton_King(world.all_levels.Grass.l_EntitiesLava.all_SkeletonKing[0], lava);

        _Checkpoint.load(_Checkpoint.ACTIVE == null ? 0 : _Checkpoint.ACTIVE.id);

        music.play(true);

        hud.onLevelStart();
        Process.resizeAll();
    }

	/** Called when CDB file changes on disk **/
	@:allow(assets.Assets)
	function onCdbReload() {
		hud.notify("CDB reloaded");
	}


	/** Called when LDtk file changes on disk **/
	@:allow(assets.Assets)
	function onLdtkReload() {
		hud.notify("LDtk reloaded");
		if( level!=null )
			startLevel();
	}

	/** Window/app resize event **/
	override function onResize() {
		super.onResize();
        scroller.setScale(Const.SCALE);
	}


	/** Garbage collect any Entity marked for destruction. This is normally done at the end of the frame, but you can call it manually if you want to make sure marked entities are disposed right away, and removed from lists. **/
	public function garbageCollectEntities() {
		if( Entity.GC==null || Entity.GC.length==0 )
			return;

		for(e in Entity.GC)
			e.dispose();
		Entity.GC = [];
	}

	/** Called if game is destroyed, but only at the end of the frame **/
	override function onDispose() {
		super.onDispose();

		fx.destroy();
        fog.destroy();
		for(e in Entity.ALL)
			e.destroy();
		garbageCollectEntities();
	}


	/**
		Start a cumulative slow-motion effect that will affect `tmod` value in this Process
		and all its children.

		@param sec Realtime second duration of this slowmo
		@param speedFactor Cumulative multiplier to the Process `tmod`
	**/
	public function addSlowMo(id:String, sec:Float, speedFactor=0.3) {
		if( slowMos.exists(id) ) {
			var s = slowMos.get(id);
			s.f = speedFactor;
			s.t = M.fmax(s.t, sec);
		}
		else
			slowMos.set(id, { id:id, t:sec, f:speedFactor });
	}


	/** The loop that updates slow-mos **/
	final function updateSlowMos() {
		// Timeout active slow-mos
		for(s in slowMos) {
			s.t -= utmod * 1/Const.FPS;
			if( s.t<=0 )
				slowMos.remove(s.id);
		}

		// Update game speed
		var targetGameSpeed = 1.0;
		for(s in slowMos)
			targetGameSpeed*=s.f;
		curGameSpeed += (targetGameSpeed-curGameSpeed) * (targetGameSpeed>curGameSpeed ? 0.2 : 0.6);

		if( M.fabs(curGameSpeed-targetGameSpeed)<=0.001 )
			curGameSpeed = targetGameSpeed;
	}


	/**
		Pause briefly the game for 1 frame: very useful for impactful moments,
		like when hitting an opponent in Street Fighter ;)
	**/
	public inline function stopFrame() {
		ucd.setS("stopFrame", 0.2);
	}


	/** Loop that happens at the beginning of the frame **/
	override function preUpdate() {
		super.preUpdate();

		for(e in Entity.ALL) if( !e.destroyed ) e.preUpdate();
	}

	/** Loop that happens at the end of the frame **/
	override function postUpdate() {
		super.postUpdate();

		// Update slow-motions
		updateSlowMos();
		baseTimeMul = ( 0.2 + 0.8*curGameSpeed ) * ( ucd.has("stopFrame") ? 0.3 : 1 );
		Assets.tiles.tmod = tmod;

		// Entities post-updates
		for(e in Entity.ALL) if( !e.destroyed ) e.postUpdate();

		// Entities final updates
		for(e in Entity.ALL) if( !e.destroyed ) e.finalUpdate();

		// Dispose entities marked as "destroyed"
		garbageCollectEntities();
	}


	/** Main loop but limited to 30 fps (so it might not be called during some frames) **/
	override function fixedUpdate() {
		super.fixedUpdate();

		// Entities "30 fps" loop
		for(e in Entity.ALL) if( !e.destroyed ) e.fixedUpdate();
	}


	/** Main loop **/
	override function update() {
		super.update();

		// Entities main loop
		for(e in Entity.ALL) if( !e.destroyed ) e.update();


		// Global key shortcuts
		if( !App.ME.anyInputHasFocus() && !ui.Modal.hasAny() && !ca.locked() ) {
			#if debug
            if (ca.isKeyboardPressed(K.TAB))
                ui.Console.ME.show();
            // Attach debug drone (CTRL-SHIFT-D)
			if( ca.isKeyboardPressed(K.D) && ca.isKeyboardDown(K.CTRL) && ca.isKeyboardDown(K.SHIFT) )
				new DebugDrone(); // <-- HERE: provide an Entity as argument to attach Drone near it
			#end

			// Restart whole game
			if( ca.selectPressed() )
				App.ME.startGame();

		}
	}

    override function pause() {
        super.pause();

        music.pause();
    }

    override function resume() {
        super.resume();

        music.resume();
    }
}

