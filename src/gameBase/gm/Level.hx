package gm;

import gm.en._Checkpoint;

class Level extends dn.Process {
	var game(get,never) : Game; inline function get_game() return Game.ME;
	var fx(get,never) : Fx; inline function get_fx() return Game.ME.fx;

	/** Level grid-based width**/
	public var cWid(get,never) : Int; inline function get_cWid() return col.cWid;

	/** Level grid-based height **/
	public var cHei(get,never) : Int; inline function get_cHei() return col.cHei;

	/** Level pixel width**/
	public var pxWid(get,never) : Int; inline function get_pxWid() return cWid*Const.GRID;

	/** Level pixel height**/
	public var pxHei(get,never) : Int; inline function get_pxHei() return cHei*Const.GRID;

    static var RENDERED = false;
    public var music(get,never) : gm.MusicManager; inline function get_music() return Game.ME.music;
    public var layers : Map<String, ldtk.Layer_IntGrid_AutoLayer>;
    public var col : ldtk.Layer_IntGrid_AutoLayer;
    public var bg : ldtk.Layer_IntGrid_AutoLayer;
    public var isLava : Bool = true;

	public var data : World_Level;
	var tilesetSource : h2d.Tile;

	var marks : Map< LevelMark, Map<Int,Bool> > = new Map();
	var invalidated = true;

    public static var COLLISIONS_GRASS_LAYER = 'collisions_grass';
    public static var BACKGROUND_GRASS_LAYER = 'background_grass';
    public static var COLLISIONS_LAVA_LAYER = 'collisions_lava';
    public static var BACKGROUND_LAVA_LAYER = 'background_lava';
    public static var BOSS_ZONE_LAVA_LAYER = 'boss_zone_lava';

	public function new(ldtkLevel:World.World_Level) {
		super(Game.ME);

		createRootInLayers(Game.ME.scroller, Const.DP_BG);
		data = ldtkLevel;
		tilesetSource = hxd.Res.levels.tiles.toTile();

        layers = [
            COLLISIONS_GRASS_LAYER => ldtkLevel.l_CollisionsGrass,
            BACKGROUND_GRASS_LAYER => ldtkLevel.l_BackgroundGrass,
            COLLISIONS_LAVA_LAYER => ldtkLevel.l_CollisionsLava,
            BACKGROUND_LAVA_LAYER => ldtkLevel.l_BackgroundLava,
            BOSS_ZONE_LAVA_LAYER => ldtkLevel.l_BossZoneLava,
        ];

        music.register(hxd.Res.music.music1_2, () -> col.identifier == layers.get(Level.COLLISIONS_LAVA_LAYER).identifier);
        music.register(hxd.Res.music.music1_1, () -> col.identifier == layers.get(Level.COLLISIONS_GRASS_LAYER).identifier);
	}

    public function initFromCheckpoint() {
        var wasIsLava = isLava;
        var isLava = _Checkpoint.ACTIVE.isLava();

        if (RENDERED && wasIsLava == isLava) {
            col = isLava ? layers.get(Level.COLLISIONS_LAVA_LAYER) : layers.get(Level.COLLISIONS_GRASS_LAYER);
            bg = isLava ? layers.get(Level.BACKGROUND_LAVA_LAYER) : layers.get(Level.BACKGROUND_GRASS_LAYER);
        } else
            swap();

        RENDERED = true;
    }

	override function onDispose() {
		super.onDispose();
		data = null;
		tilesetSource = null;
		marks = null;
	}

	/** TRUE if given coords are in level bounds **/
	public inline function isValid(cx,cy) return cx>=0 && cx<cWid && cy>=0 && cy<cHei;

	/** Gets the integer ID of a given level grid coord **/
	public inline function coordId(cx,cy) return cx + cy*cWid;

	/** Ask for a level render that will only happen at the end of the current frame. **/
	public inline function invalidate() {
		invalidated = true;
	}

	/** Return TRUE if mark is present at coordinates **/
	public inline function hasMark(mark:LevelMark, cx:Int, cy:Int) {
		return !isValid(cx,cy) || !marks.exists(mark) ? false : marks.get(mark).exists( coordId(cx,cy) );
	}

	/** Enable mark at coordinates **/
	public function setMark(mark:LevelMark, cx:Int, cy:Int) {
		if( isValid(cx,cy) && !hasMark(mark,cx,cy) ) {
			if( !marks.exists(mark) )
				marks.set(mark, new Map());
			marks.get(mark).set( coordId(cx,cy), true );
		}
	}

	/** Remove mark at coordinates **/
	public function removeMark(mark:LevelMark, cx:Int, cy:Int) {
		if( isValid(cx,cy) && hasMark(mark,cx,cy) )
			marks.get(mark).remove( coordId(cx,cy) );
	}

    public inline function hasCollision(cx:Int , cy:Int, layer: ldtk.Layer_IntGrid_AutoLayer=null) : Bool {
        return (layer != null ? layer.getInt(cx,cy) : col.getInt(cx,cy)) > 0;
    }

	/** Return TRUE if "Collisions" layer contains a collision value **/
	public inline function _hasCollision(cx,cy) : Bool {
		return !isValid(cx,cy) ? true : col.getInt(cx,cy)==1;
	}

	/** Render current level**/
	function render() {
		// Placeholder level render
		root.removeChildren();

		// var tg = new h2d.TileGroup(tilesetSource, root);

		// var layer = data.l_Collisions;
		// for( autoTile in layer.autoTiles ) {
		// 	var tile = layer.tileset.getAutoLayerTile(autoTile);
		// 	tg.add(autoTile.renderX, autoTile.renderY, tile);
		// }

        bg.render(Assets.getTiles('background', root));
        col.render(Assets.getTiles('collisions', root));

        for (entity in game.entities.get('grass'))
            entity.entityVisible = !isLava;

        for (entity in game.entities.get('lava'))
            entity.entityVisible = isLava;
	}

    public function swap(): Void {
        col = !isLava ? layers.get(Level.COLLISIONS_LAVA_LAYER) : layers.get(Level.COLLISIONS_GRASS_LAYER);
        bg = !isLava ? layers.get(Level.BACKGROUND_LAVA_LAYER) : layers.get(Level.BACKGROUND_GRASS_LAYER);
        isLava = col.identifier == layers.get(COLLISIONS_LAVA_LAYER).identifier;
        game.music.swap();
        invalidate();
    }

	override function postUpdate() {
		super.postUpdate();

		if( invalidated ) {
			invalidated = false;
			render();
		}
	}
}
