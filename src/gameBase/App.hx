/**
	"App" class takes care of all the top-level stuff in the whole application. Any other Process, including Game instance, should be a child of App.
**/

class App extends dn.Process {
	public static var ME : App;

	/** 2D scene **/
	public var scene(default,null) : h2d.Scene;

	/** Used to create "ControllerAccess" instances that will grant controller usage (keyboard or gamepad) **/
	public var controller : dn.heaps.Controller;

	/** Controller Access created for Main & Boot **/
	public var ca : dn.heaps.Controller.ControllerAccess;

	private var graphicsTitle : h2d.Graphics;

	public function new(s:h2d.Scene) {
		super();
		ME = this;
		scene = s;
        createRoot(scene);

		initEngine();
		initAssets();
		initController();

		// Create console (open with [²] key)
		new ui.Console(Assets.fontTiny, scene); // init debug console

		// Optional screen that shows a "Click to start/continue" message when the game client looses focus
		new tools.GameFocusHelper(scene, Assets.fontMedium);

		graphicsTitle = new h2d.Graphics(s);
        var titleTile = Assets.getTile('title');
        graphicsTitle.drawTile(scene.width / 2 - titleTile.width / 2, scene.height / 2 - titleTile.height / 2, titleTile);
        scene.add(graphicsTitle, 0);

		hxd.System.setNativeCursor(Hide);

        #if debug
        Console.ME.setFlag('cd', true);
        Console.ME.setFlag('affect', true);
        #end
	}



	/** Start game process **/
	public function startGame() {
		if( Game.exists() ) {
			// Kill previous game instance first
			Game.ME.destroy();
            Game.DEATH++;
			dn.Process.updateAll(1); // ensure all garbage collection is done
			_createGameInstance();
			hxd.Timer.skip();
		}
		else {
			// Fresh start
			scene.removeChild(graphicsTitle);
        	graphicsTitle = null;
			delayer.addF( ()->{
				_createGameInstance();
				hxd.Timer.skip();
			}, 1 );
		}
	}

	final function _createGameInstance() {
		engine.backgroundColor = 0x7D96AA;
		new Game(); // <---- Uncomment this to start an empty Game instance
		// new sample.SampleGame(); // <---- Uncomment this to start the Sample Game instance
	}


	public function anyInputHasFocus() {
		return Console.ME.isActive();
	}


	/**
		Initialize low level stuff, before anything else
	**/
	function initEngine() {
		// Engine settings
        #if( hl && !debug )
        engine.fullScreen = true;
        #end

		// Heaps resource management
		#if( hl && debug )
			hxd.Res.initLocal();
			hxd.res.Resource.LIVE_UPDATE = true;
        #else
      		hxd.Res.initEmbed();
        #end

		// Sound manager (force manager init on startup to avoid a freeze on first sound playback)
		hxd.snd.Manager.get();
		hxd.Timer.skip(); // needed to ignore heavy Sound manager init frame

		// Framerate
		hxd.Timer.smoothFactor = 0.4;
		hxd.Timer.wantedFPS = Const.FPS;
		// TODO readd dn.Process.FIXED_UPDATE_FPS = Const.FIXED_UPDATE_FPS;
	}


	/** Init app assets **/
	function initAssets() {
		// Init game assets
		Assets.init();

		// Init lang data
		Lang.init("en");
	}


	/** Init game controller and default key bindings **/
	function initController() {
		controller = new dn.heaps.Controller(scene);
		ca = controller.createAccess("main");
        controller.bind(AXIS_LEFT_X_NEG, K.LEFT, K.Q);
        controller.bind(AXIS_LEFT_X_POS, K.RIGHT, K.D);
        controller.bind(AXIS_LEFT_Y_NEG, K.DOWN, K.S);
        controller.bind(AXIS_LEFT_Y_POS, K.UP, K.Z);
        controller.bind(X, K.E);
        controller.bind(Y, K.F);
        controller.bind(A, K.UP, K.Z);
        controller.bind(B, K.SPACE);
        controller.bind(SELECT, K.BACKSPACE);
        controller.bind(START, K.ENTER, K.NUMPAD_ENTER);
	}


	/** Return TRUE if an App instance exists **/
	public static inline function exists() return ME!=null && !ME.destroyed;

	/** Close the app **/
	public function exit() {
		destroy();
	}

	override function onDispose() {
		super.onDispose();

		#if hl
		hxd.System.exit();
		#end
	}

    override function update() {
		Assets.tiles.tmod = tmod;

		if (graphicsTitle != null) {
            var titleTile = Assets.getTile('title');
            graphicsTitle.clear();
            graphicsTitle.drawTile(scene.width / 2 - titleTile.width / 2, scene.height / 2 - titleTile.height / 2, titleTile);
        }

        if (Game.ME == null && (ca.startPressed() || hxd.Key.isPressed(K.ENTER))) {
			startGame();
        }

        #if debug
        engine.fullScreen = ui.Console.ME.hasFlag("fullscreen");
        #end

        super.update();
    }

    override function pause() {
        if (Game.ME != null)
			Game.ME.pause();
    }

    override function resume() {
        if (Game.ME != null)
			Game.ME.resume();
    }
}
