package tools;

import hxd.Key;
import dn.heaps.GameFocusHelper as BaseGameFocusHelper;

class GameFocusHelper extends BaseGameFocusHelper {
    public static var ME : GameFocusHelper;
    public var ca : dn.heaps.Controller.ControllerAccess;
    public var menu : ui.Menu;

    public function new(s:h2d.Scene, font:h2d.Font) {
        super(s, font);

        #if js
        resumeGame();
        #end

        ME = this;
        ca = App.ME.controller.createAccess('game_focus_helper');
        menu = new ui.Menu();
    }

    public static function suspend() {
        ME.suspendGame();
    }

    override function update() {
        if (Game.ME == null)
            return;

        super.update();

        if (ca.isKeyboardPressed(Key.ESCAPE))
            suspended ? resumeGame() : suspendGame();

        #if !js
        if ( suspended && ca.startPressed() )
            Sys.exit(0);
        #end

        if ( suspended && ca.selectPressed() ) {
            Game.DIFFICULTY = (Game.DIFFICULTY == 2) ? 1 : 2;
            menu.updateDifficulty();
            Game.ME.hero.addBonusLife((Game.DIFFICULTY == 2) ? -5 : 5);
        }
    }

    override function resumeGame() {
        super.resumeGame();

        if( suspended )
            return;

        // remove parent h2d.Interactive
        var lastChild = root.getChildAt(root.numChildren - 1);
        if (Std.isOfType(lastChild, h2d.Interactive))
            root.removeChild(lastChild);
    }

    override function suspendGame() {
        super.suspendGame();

        if (!suspended)
            return;

        // remove parent h2d.Text
        var lastChild = root.getChildAt(root.numChildren - 1);
        if (Std.isOfType(lastChild, h2d.Text))
            root.removeChild(lastChild);

        if (menu != null)
            menu.draw(root);
    }
}
