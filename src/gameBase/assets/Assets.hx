package assets;

import hxd.res.Resource;
import hxd.fs.FileEntry;
import dn.heaps.slib.*;

/**
	This class centralizes all assets management (ie. art, sounds, fonts etc.)
**/
class Assets {
	// Fonts
	public static var fontPixel : h2d.Font;
	public static var fontTiny : h2d.Font;
	public static var fontSmall : h2d.Font;
	public static var fontMedium : h2d.Font;
	public static var fontLarge : h2d.Font;

	// Sprite atlas
	public static var tiles : SpriteLib;

	// LDtk world data
	public static var worldData : World;

	static var spriteLibList : Map<String,SpriteLib> = [];
	static var tileList : Map<String,h2d.Tile> = [];

	static var _initDone = false;
	public static function init() {
		if( _initDone )
			return;
		_initDone = true;

		// Fonts
		fontPixel = hxd.Res.fonts.minecraftiaOutline.toFont();
		fontTiny = hxd.Res.fonts.barlow_condensed_medium_regular_9.toFont();
		fontSmall = hxd.Res.fonts.barlow_condensed_medium_regular_11.toFont();
		fontMedium = hxd.Res.fonts.barlow_condensed_medium_regular_17.toFont();
		fontLarge = hxd.Res.fonts.barlow_condensed_medium_regular_32.toFont();

		// Atlas
		tiles = dn.heaps.assets.Atlas.load("atlas/tiles.atlas");

		// LDtk init & parsing
		worldData = new World();

		// LDtk file hot-reloading
		#if debug
		var res = try hxd.Res.load(worldData.projectFilePath.substr(4)) catch(_) null; // assume the LDtk file is in "res/" subfolder
		if( res!=null )
			res.watch( ()->{
				// Only reload actual updated file from disk after a short delay, to avoid reading a file being written
				App.ME.delayer.cancelById("ldtk");
				App.ME.delayer.addS("ldtk", function() {
					worldData.parseJson( res.entry.getText() );
					if( Game.exists() )
						Game.ME.onLdtkReload();
				}, 0.2);
			});
		#end
	}


	public static function update(tmod) {
		tiles.tmod = tmod;
	}

	public static function getLib(id: String, zeroBased: Bool = true): SpriteLib {
        if (!spriteLibList.exists(id)) {
            spriteLibList.set(id, dn.heaps.assets.Atlas.load('atlas/${id}.atlas', () -> {}, zeroBased ? null : ['*']));
        }

        return spriteLibList.get(id);
    }

    public static function getTile(id: String): h2d.Tile {
        if (!tileList.exists(id)) {
            tileList.set(id, hxd.Res.load('world/${id}.png').toTile());
        }

        return tileList.get(id);
    }

    public static function getTiles(id: String, ?parent: h2d.Object): h2d.TileGroup {
        return new h2d.TileGroup(Assets.getTile(id), parent);
    }
}
