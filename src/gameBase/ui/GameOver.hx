package ui;

class GameOver extends dn.Process {
	public var game(get,never) : Game; inline function get_game() return Game.ME;
    public var flow : h2d.Flow;

	public function new() {
		super(Game.ME);

		createRootInLayers(game.root, Const.DP_UI);
		root.filter = new h2d.filter.ColorMatrix(); // force pixel perfect rendering

		flow = new h2d.Flow(root);
        flow.padding = 125;
        flow.minWidth = Game.ME.w();
        flow.minHeight = Game.ME.h();

        var bg = new h2d.Graphics(flow);
        bg.beginFill(0x000000, 0.8);
        bg.drawRect(0, 0, flow.innerWidth, flow.innerHeight);

        var inner = new h2d.Flow(bg);
        inner.layout = Vertical;
        inner.minWidth = flow.innerWidth;
        inner.minHeight = flow.innerHeight;
        inner.horizontalAlign = h2d.Flow.FlowAlign.Middle;
        inner.verticalAlign = h2d.Flow.FlowAlign.Middle;
        inner.horizontalSpacing = 10;
        inner.verticalSpacing = 10;

        var row1 = new h2d.Text(Assets.fontMedium, inner);
        row1.text = 'GAME OVER';
        var row2 = new h2d.Text(Assets.fontMedium, inner);
        row2.text = 'PRESS START/ENTER TO CONTINUE';

        game.fog.pause();
    }

    override public function update() {
        if( game.ca.startPressed() )
            App.ME.startGame();
    }
}
