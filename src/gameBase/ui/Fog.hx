package ui;

import dn.Process;

class Fog extends Process {
    public var spr : HSprite;

    var level(get,never) : Level; inline function get_level() return Game.ME.level;
    var hero(get,never) : gm.en.Hero; inline function get_hero() return Game.ME.hero;
    var entity(get,never) : Entity; inline function get_entity() return Game.ME.entity;
    var scroller(get, never): h2d.Layers; inline function get_scroller() return Game.ME.scroller;

    var x(get,set) : Float; inline function get_x() return spr.x; inline function set_x(x:Float) return spr.x = x;
    var y(get,set) : Float; inline function get_y() return spr.y; inline function set_y(y:Float) return spr.y = y;
    var dx : Float = 0;
    var dy : Float = 0;

    var ratio: Int = 8;

    public function new() {
        super();

        createRootInLayers(Game.ME.root, Const.DP_FX_FRONT);

        spr = Assets.getLib('fog').h_get('fog');
        spr.colorize(0, 1);
        spr.alpha = 0;
        spr.visible = false;

        root.add(spr, Const.DP_FX_FRONT);
    }

    override function onResize() {
        if (spr != null) {
            var width = w();
            var height = h();
            spr.scaleX = 30 * width/spr.tile.width;
            spr.scaleY = 30 * height/spr.tile.height;
            spr.setCenterRatio();
            spr.x = width/2;
            spr.y = height/2;
        }
    }

    override function postUpdate() {
        var lavaDuration = lavaDuration();
        var wasVisible = spr.visible;

        if (level.isLava) {
            spr.alpha += (1 - spr.alpha) * 0.02;
            var scale = Math.pow(1 / ratio, 1 / (Const.FPS * (lavaDuration+1) * tmod));
            spr.scale(scale);

            var s = 0.006;
            var deadZone = 5;
            var tx = hero.centerX * Game.ME.camera.zoom + scroller.x;
            var ty = hero.centerY * Game.ME.camera.zoom + scroller.y;

            var d = M.dist(x,y, tx, ty);
            if( d>=deadZone ) {
                var a = Math.atan2( ty-y, tx-x );
                dx += Math.cos(a) * (d-deadZone) * s * tmod;
                dy += Math.sin(a) * (d-deadZone) * s * tmod;
            }

            var frict = 0.89;
            x += dx*tmod;
            dx *= Math.pow(frict,tmod);

            y += dy*tmod;
            dy *= Math.pow(frict,tmod);
        } else {
            spr.alpha = 0;
        }

        spr.visible = spr.alpha > 0.1;

        if (!wasVisible && spr.visible) {
            cd.setMs('fogDuration', lavaDuration * 1000, () -> hero.kill(entity));
        } else if (wasVisible && !spr.visible) {
            cd.unset('fogDuration');
            onResize();
        }
    }

    inline function lavaDuration(): Int {
        var ring = gm.en.Ring.ACTIVE != null ? gm.en.Ring.ACTIVE.lavaDuration : 0;
        var lavaDuration =  gm.en._Checkpoint.ACTIVE != null ? Std.int(M.fmax(gm.en._Checkpoint.ACTIVE.lavaDuration, ring)) : ring;
        if (Game.DIFFICULTY == 1) lavaDuration *= 2;

        return lavaDuration;
    }

    public function blink(c:UInt, a:Float, ?t=0.1) {
        if (root == null)
            return;

        var e = new h2d.Bitmap(h2d.Tile.fromColor(c,1,1,a));
        root.add(e, Const.DP_FX_FRONT);
        e.scaleX = w();
        e.scaleY = h();
        e.blendMode = Max;
        Game.ME.tw.createS(a, 0, t).end(() -> e.remove());
    }
}
