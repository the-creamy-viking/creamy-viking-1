package ui;

class Life extends dn.Process {
	public var obj : h2d.Object;

	var value : Float = 1.0;
	var lastValue : Float = 1.0;
	var width : Int = 1;
	var height : Int = 1;
	var visible : Bool = false;
	var bar : h2d.Graphics;
	var lifes : Array<h2d.Graphics> = [];
	var cAdd : h3d.Vector = new h3d.Vector();

	public function new(x, y, wid, hei) {
		super(Game.ME);
		createRoot(Game.ME.hud.root);

		obj = new h2d.Object(root);
		obj.visible = false;
		obj.x = x;
		obj.y = y;
		width = wid;
		height = hei;

		var outline = new h2d.Graphics(obj);
		outline.beginFill(0x000000,1);
		outline.drawRect(0,0,wid+2,hei+2);

		var bg = new h2d.Graphics(obj);
		bg.beginFill(0x371E0F,1);
		bg.drawRect(1,1,wid,hei);

		bar = new h2d.Graphics(obj);
		bar.beginFill(0xE62639,1);
		bar.drawRect(1,1,wid,hei);
		bar.colorAdd = cAdd;
	}

	public function set(v:Float) {
		lastValue = value;
		value = v;
	}

	public function show() {
		obj.visible = true;
	}

	public function hide() {
		obj.visible = false;
	}

	override public function postUpdate() {
		super.postUpdate();
		if( cd.has("shake") )
			root.y += Math.cos(ftime*0.7)*2 * cd.getRatio("shake");

		if (cd.has("dissociate")) {
			for (life in lifes) {
				if (life.alpha < 0.1) {
					life.clear();
					lifes.remove(life);
				} else {
					life.y++;
					life.alpha *= 0.9;
				}
			}
		}

		bar.scaleX += ( M.fclamp(value,0,1) - bar.scaleX ) * 0.4;
	}

	public function blink() {
		cd.setS("shake", 1);
		cAdd.r = 0.9;
		cAdd.g = 0.9;
		cAdd.b = 0.9;
	}

	public function dissociate() {
		cd.setS("dissociate", 1);

		var life = new h2d.Graphics(obj);
		var v = width * (lastValue - value);
		var x = width * value + 1;

		life.beginFill(0xffffff,1);
		life.drawRect(x, 1, v, height);

		lifes.push(life);
	}

	override public function update() {
        super.update();

        if (cAdd.r > 0.01)
            cAdd.r*=0.8;
        if (cAdd.g > 0.01)
		    cAdd.g*=0.8;
        if (cAdd.b > 0.01)
            cAdd.b*=0.8;
	}
}
