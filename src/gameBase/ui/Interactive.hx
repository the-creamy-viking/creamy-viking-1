package ui;

typedef InteractiveEvent = {
	x: Float,
	y: Float,
}

class Interactive extends dn.Process {
    public static var ME : Interactive;
    static var THRESHOLD = 30;

    var ca : dn.heaps.Controller.ControllerAccess;

    var interactiveEvent : InteractiveEvent = { x: -1, y: -1 };
    var buttons : Map<String, Bool> = [];

	public function new() {
        super(Game.ME);

        ME = this;
        ca = App.ME.controller.createAccess('interactive');

        if (!hxd.System.getValue(IsMobile))
            return;

        var scene = App.ME.scene;
        var halfWidth = Std.int(scene.width / 2);
        var halfHeight = Std.int(scene.height / 2);

        handleButton(createInteractive(halfWidth, halfHeight, 0, 0), 'x');
        handleMove(createInteractive(halfWidth, halfHeight, 0, halfHeight));
        handleButton(createInteractive(halfWidth, halfHeight, halfWidth, 0), 'b');
        handleButton(createInteractive(halfWidth, halfHeight, halfWidth, halfHeight), 'a');
    }

    function handleMove(interactive: h2d.Interactive, _: Int = 10000) {
        interactive.onPush = (event: hxd.Event) -> {
            interactiveEvent.x = event.relX;
            interactiveEvent.y = event.relY;
        }

		interactive.onMove = (event: hxd.Event) -> {
            cd.unset('leftDown');
            cd.unset('rightDown');

            isMoving(event.relX, interactiveEvent.x, () -> cd.setF('leftDown', _), () -> cd.setF('rightDown', _));
            isMoving(event.relY, interactiveEvent.y, () -> cd.setF('upDown', _), () -> cd.setF('downDown', _));
        }

        interactive.onRelease = (event: hxd.Event) -> {
            cd.unset('leftDown');
            cd.unset('rightDown');
        }
    }

    function isMoving(next: Float, prev: Float, negativeCb: Void->Void, positiveCb: Void->Void) {
        if (M.fabs(next - prev) > THRESHOLD) {
            if (next - prev < 0)
                negativeCb();
            else
                positiveCb();
        }
    }

    function handleButton(interactive: h2d.Interactive, button: String) {
        interactive.onPush = (event: hxd.Event) -> {
            buttons[button] = true;
        }

        interactive.onRelease = (event: hxd.Event) -> {
            buttons[button] = false;
        }
    }

    function createInteractive(width: Int, height: Int, x: Int = 0, y: Int = 0): h2d.Interactive {
        var object = new h2d.Object(App.ME.scene);
        object.x = x;
        object.y = y;

        return new h2d.Interactive(width, height, object);
    }

    override function onDispose() {
        ca.dispose();
    }

    public function leftDown() {
        return ca.leftDown() || cd.has('leftDown');
    }

    public function rightDown() {
        return ca.rightDown() || cd.has('rightDown');
    }

    public function aDown() {
        return ca.aDown() || isDown('a');
    }

    public function bDown() {
        return ca.bDown() || isDown('b');
    }

    public function xDown() {
        return ca.xDown() || isDown('x');
    }

    private function isDown(button: String): Bool {
        return buttons.exists(button) && buttons.get(button);
    }
}
