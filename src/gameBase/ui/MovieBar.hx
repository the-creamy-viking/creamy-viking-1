package ui;

class MovieBar extends dn.Process {
	var barHei : Int = 90;
	var bar : h2d.Graphics;
	var started : Bool = false;
	var stopped : Bool = false;
	var value : Int; // 1 top -1 bottom
	var currentValue : Int = 0;

	public function new(_value: Int) {
		super(Game.ME);
		value = _value;
		createRoot(Game.ME.root);

        var s = new h2d.Object(root);
		var barWid = w();
		s.x = 0;
		s.y = value == 1 ? 0 : h();
		bar = new h2d.Graphics(s);
		bar.beginFill(0x000000,1);
		bar.drawRect(bar.x,bar.y,barWid,barHei);
		bar.scaleY = currentValue;
	}

	override public function postUpdate() {
		super.postUpdate();

		if (bar.scaleY != currentValue)
			bar.scaleY += ( M.fclamp(currentValue,-1,1) - bar.scaleY ) * 0.04;
	}

	public function start() {
		currentValue = value;
	}

	public function stop() {
		currentValue = 0;
	}
}
