package ui;

import uuid.Uuid;

typedef Score = {
    var id: String;
    var name: String;
    var time: Int;
    var death: String;
    var uuid: String;
    var difficulty: String;
}

class Leaderboard extends dn.Process {
	public var game(get,never) : Game; inline function get_game() return Game.ME;
    public var flow : h2d.Flow;
    public var uuid : String;

	public function new() {
		super(Game.ME);

		createRootInLayers(game.root, Const.DP_UI);
		root.filter = new h2d.filter.ColorMatrix(); // force pixel perfect rendering

        uuid = Uuid.v1();
	}

    public function add(?name: String) {
        var time = Game.TIME;

        if (name == null)
            name = #if js uuid #else sys.net.Host.Host.localhost() #end;

        var http = http();
        http.setParameter('name', name);
        http.setParameter('time', Std.string(M.round(time)));
        http.setParameter('death', Std.string(Game.DEATH));
        http.setParameter('uuid', uuid);
        http.setParameter('difficulty', Game.DIFFICULTY == 1 ? 'easy' : 'normal');
        http.setParameter('mode', #if debug 'debug' #else 'release' #end);
        http.onStatus = status -> list();
        http.request(true);
    }

    private function list() {
        var http = http();
        http.setParameter('_sort', 'time,id');
        http.setParameter('mode', #if debug 'debug' #else 'release' #end);
        http.onData = data -> draw(haxe.Json.parse(data));
        http.request();
    }

    private function draw(scores: Array<Score>) {
		flow = new h2d.Flow(root);
        flow.padding = 125;
        flow.minWidth = Game.ME.w();
        flow.minHeight = Game.ME.h();

        var bg = new h2d.Graphics(flow);
        bg.beginFill(0x000000, 0.8);
        bg.drawRect(0, 0, flow.innerWidth, flow.innerHeight);

        var inner = new h2d.Flow(bg);
        inner.layout = Vertical;

        var title = new h2d.Flow(inner);
        title.minWidth = flow.innerWidth;
        title.horizontalAlign = h2d.Flow.FlowAlign.Middle;
        title.padding = 50;

        var text = new h2d.Text(Assets.fontMedium, title);
        text.text = 'LEADERBOARD';

        var _score = null;
        var _idx = 0;

        for (idx => score in scores) {
            if (score.uuid == uuid) {
                _score = score;
                _idx = idx;
                break;
            }
        }

        var foundSelf = false;

        for (idx => score in scores.slice(0, 8)) {
            if (idx != 7)
                drawRaw(inner, score, idx, score.uuid == uuid);
            else
                drawRaw(inner, foundSelf ? score : _score, foundSelf ? idx : _idx, _score != null && score.uuid == _score.uuid || !foundSelf);

            if (_score == null || score.uuid == _score.uuid)
                foundSelf = true;
        }
    }

    private function drawRaw(inner: h2d.Flow, score: Score, idx: Int, self: Bool = false) {
        var row = new h2d.Flow(inner);
        row.maxWidth = Std.int(flow.innerWidth / 3);
        row.padding = 10;

        var index = new h2d.Flow(row);
        index.horizontalAlign = h2d.Flow.FlowAlign.Middle;
        index.fillWidth = true;

        var indexText = new h2d.Text(Assets.fontSmall, index);
        indexText.text = '#${idx + 1}';
        if (self)
            indexText.textColor = 0xff8f00;

        var name = new h2d.Flow(row);
        name.horizontalAlign = h2d.Flow.FlowAlign.Middle;
        name.fillWidth = true;

        var nameText = new h2d.Text(Assets.fontSmall, name);
        nameText.text = score.name;
        if (self)
            nameText.textColor = 0xff8f00;

        var time = new h2d.Flow(row);
        time.horizontalAlign = h2d.Flow.FlowAlign.Middle;
        time.fillWidth = true;

        var timeText = new h2d.Text(Assets.fontSmall, time);
        timeText.text = '${score.time > 60 ? '${M.floor(score.time / 60)}m ${score.time % 60}s' : '${score.time}s'} (${score.death})${score.difficulty == 'easy' ? ' *' : ''}';
        if (self)
            timeText.textColor = 0xff8f00;
    }

    private function http() {
        var http = new haxe.Http('http://tcv.qkdreyer.dev:3000/scores');
        var epoch = Std.int(Date.now().getTime()/1000)|0;
        var token = haxe.crypto.Sha256.encode('tcv@password:${epoch}');

        http.addHeader('Authorization', 'Bearer ${token}');

        return http;
    }
}
