package ui;

class Menu extends dn.Process {
    public var game(get,never) : Game; inline function get_game() return Game.ME;
    public var flow : h2d.Flow;
    var difficulty : h2d.Text;

    public function draw(root) {
        flow = new h2d.Flow(root);
        flow.padding = 125;
        flow.minWidth = game.w();
        flow.minHeight = game.h();

        var bg = new h2d.Graphics(flow);
        bg.beginFill(0x000000);
        bg.drawRect(0, 0, flow.innerWidth, flow.innerHeight);

        var fui = new h2d.Flow(bg);
		fui.layout = Vertical;
        fui.minWidth = flow.innerWidth;
        fui.minHeight = flow.innerHeight;
		fui.verticalSpacing = 30;
        fui.padding = 30;
        fui.horizontalAlign = h2d.Flow.FlowAlign.Middle;
        fui.verticalAlign = h2d.Flow.FlowAlign.Middle;

        var row = new h2d.Flow(fui);
        row.horizontalSpacing = 15;
        row.verticalAlign = h2d.Flow.FlowAlign.Middle;

        var row1 = new h2d.Text(Assets.fontMedium, fui);
        row1.text = 'PAUSED';
        var row2 = new h2d.Text(Assets.fontMedium, fui);
        row2.text = 'PRESS START/ENTER TO QUIT';
        var row3 = new h2d.Text(Assets.fontMedium, fui);
        row3.text = 'PRESS SELECT/BACKSPACE TO TOGGLE DIFFICULTY';

        difficulty = new h2d.Text(Assets.fontMedium, fui);
        updateDifficulty();

        var musicBitmap = new h2d.Bitmap(Assets.getTile('music'), row);
        musicBitmap.scaleX = 0.08;
        musicBitmap.scaleY = 0.08;
        var music = new h2d.Slider(150, 30, row);
        music.value = gm.MusicManager.MAX_VOLUME;
        music.onChange = () -> game.music.volume = music.value;
    }

    public function updateDifficulty() {
        difficulty.text = 'CURRENT DIFFICULTY: [${Game.DIFFICULTY == 1 ? 'EASY' : 'NORMAL'}]';
    }
}
