package sample;

/**
	This small class just creates a SamplePlayer instance in current level
**/
class SampleGame extends Game {
	public function new() {
		super();

	}

	override function startLevel() {
		super.startLevel();
		new SamplePlayer();
	}
}

